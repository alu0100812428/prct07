#añadir clase
Node = Struct.new(:value, :next)
class List
  attr_accessor :head, :size

  def initialize
    @head = nil
    @size = 0
  end

  def insert(value)
    node = Node.new(value)
    if @head.nil?
        @head = node
    else
      node.next = @head
      @head = node
    end 
    @size +=1
  end
  
  def extract
    @head=@head.next
    @size -=1
  end
  

  def mostrar()
    #@head.value
    nodo_actual = Node.new(@head.value)
    nodo_actual.next = @head.next
    aux=[]
    while nodo_actual.next != nil
        aux.push("#{nodo_actual.value}")
        nodo_actual = nodo_actual.next
    end
    aux.push(nodo_actual.value)
    aux.join(",")
  end
  
  def size()
    count 
  end

end

class Node
  attr_accessor :value, :next

  def initialize(dato)
    @next = nil 
    @value = dato
  end 
end